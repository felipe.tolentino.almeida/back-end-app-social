const axios = require('axios');
const Dev = require('../models/Dev');
const parseStringAsArray = require('../utils/parseStringAsArray');
const { findConnections , sendMessage } = require('../websocket');

module.exports = {
    async store (req, res) {
        const { github_username , techs, latitude, longitude } = req.body;

        let dev = await Dev.findOne({github_username });

        if(!dev){
            console.log('cadastra novo dev')
            const response = await axios.get(`https://api.github.com/users/${github_username}`);
            const {name = login , avatar_url , bio } = response.data;
            const tecnologias = parseStringAsArray(techs);

            const location = { 
                type: 'Point',
                coordinates: [longitude, latitude]
            } 

            dev = await Dev.create({
                github_username,
                name,
                avatar_url,
                bio,
                techs: tecnologias,
                location,
            })

            //Filtrar connections e ver quais precisamos avisar o front - no maximo 10km de distancia e as techs sejam parecidas.
            const sendSocketMessageTo = findConnections(
                {latitude, longitude},
                tecnologias
            );
            
            sendMessage(sendSocketMessageTo , 'new-dev', dev);
            console.log(sendSocketMessageTo)
        }
        return res.json({dev});
    },
    async index (req, res) {
        const devs = await Dev.find();
        return res.json(devs);
    },
    async update (req, res) {
        const { github_username , techs, latitude, longitude , bio, avatar_url} = req.body;

        if(github_username !== undefined && techs !== undefined && latitude !== undefined && longitude !== undefined && bio !== undefined && avatar_url!== undefined ){
            const tecnologias = parseStringAsArray(techs);

            var dev = await Dev.updateOne({
                github_username:{
                    $eq: github_username,
                }
            } ,
            { 
                $set: { 
                    techs: tecnologias, 
                    latitude: latitude, 
                    longitude: longitude, 
                    bio: bio,
                    avatar_url: avatar_url
                }, 
            });
        }
        return res.json({dev});
    },
    async destroy (req, res){
        const { github_username } = req.params;

        let dev = await Dev.remove({
            github_username:{
                $eq: github_username,
            } 
        },
        {
            justOne: true,
        }
        );
        return res.json({dev});
    }
}