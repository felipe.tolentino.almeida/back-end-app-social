const { Router } = require('express');
const DevController = require('./controllers/Dev');
const SearchController = require('./controllers/SearchController');

const routes = Router();

routes.get('/devs' , DevController.index);
routes.post('/devs' , DevController.store );
routes.get('/search' , SearchController.index);
routes.delete('/deleteUser/:github_username', DevController.destroy);
routes.put('/updateUser', DevController.update);

module.exports = routes ;